\NeedsTeXFormat{LaTeX2e}


% Invoice template by Globulle
% See https://framagit.org/Globulle/invoice-template-latex.git

% Shared under Licence CC-By 4.0 
% http://creativecommons.org/licenses/by/4.0/

% Project inspired from YAIT: yet another invoice template
% by Elie Roux (https://github.com/eroux/latex-yait)


\ProvidesClass{invoice}[2019/01/02 Classe personnelle, V0.1]

% Options de la classe
\DeclareOption{facture}{\def\@@invoice@type{Facture}}
\DeclareOption{devis}{\def\@@invoice@type{Devis}}

\ExecuteOptions{facture} % default option

\ProcessOptions


% classe de base

\LoadClass[a4paper, 10pt]{article}

% extensions

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage[french]{babel}

\RequirePackage[vmargin=1.5cm, hmargin=2cm]{geometry}
\RequirePackage{textcomp}

\RequirePackage[pdfencoding=auto,unicode, bookmarks=false, colorlinks=false, pdfborder={0 0 0}]{hyperref}


\pagestyle{empty}

\RequirePackage[table]{xcolor}
\RequirePackage{longtable}
\RequirePackage{tabularx}
\RequirePackage{array}
\RequirePackage{tabularray}[2023/02/10] % version higher than 2023A is required for \lTblrMeasuringBool to exist
\RequirePackage{multicol}
\RequirePackage[norule]{footmisc}
\RequirePackage{environ}
\RequirePackage{fp}
\RequirePackage[round-mode=places,
    round-precision=2,
    detect-weight=true]{siunitx}
\RequirePackage{pdftexcmds} % to check whether --shell-escape option is enabled


%%%%%%%%%%%%%%%%%%%%%
% color definitions %
%%%%%%%%%%%%%%%%%%%%%
\colorlet{headcolor}{gray!21}
\colorlet{tablecolor1}{gray!4}
\colorlet{tablecolor2}{gray!11}
\colorlet{footnotegray}{gray!90}

\let\thetitle\@title


\def\@@invoice@num{}
\newcommand{\invoicenumber}[1]{\global\def\@@invoice@num{#1}}



\def\@@company@name{}
\def\@@company@subname{}
\def\@@company@addressA{}
\def\@@company@addressB{}
\def\@@company@siren{}
\def\@@company@tel{}
\def\@@company@email{}
\def\@@bankiban{}
\def\@@bankbic{}

\newenvironment{company}{
        \newcommand{\name}[1]{\global\def\@@company@name{##1}}
        \newcommand{\complementaryname}[1]{\global\def\@@company@subname{##1}}
        \newcommand{\addressA}[1]{\global\def\@@company@addressA{##1}}
        \newcommand{\addressB}[1]{\global\def\@@company@addressB{##1}}
        \newcommand{\siren}[1]{\global\def\@@company@siren{##1}}
        \newcommand{\tel}[1]{\global\def\@@company@tel{##1}}
        \newcommand{\email}[1]{\global\def\@@company@email{##1}}
        \newcommand{\bankiban}[1]{\global\def\@@company@iban{##1}}
        \newcommand{\bankbic}[1]{\global\def\@@company@bic{##1}}
}{}

\def\@@customer@name{}
\def\@@customer@addressA{}
\def\@@customer@addressB{}
\def\@@customer@siren{}
\def\@@customer@tel{}
\def\@@customer@email{}
% \def\@@bankiban{}
% \def\@@bankbic{}
\newenvironment{customer}{
        \newcommand{\name}[1]{\global\def\@@customer@name{##1}}
        \newcommand{\addressA}[1]{\global\def\@@customer@addressA{##1}}
        \newcommand{\addressB}[1]{\global\def\@@customer@addressB{##1}}
        \newcommand{\siren}[1]{\global\def\@@customer@siren{##1}}
        \newcommand{\tel}[1]{\global\def\@@customer@tel{##1}}
        \newcommand{\email}[1]{\global\def\@@customer@email{##1}}
%         \newcommand{\bankiban}[1]{\global\def\@@bankiban{##1}}
%         \newcommand{\bankbic}[1]{\global\def\@@bankbic{##1}}
}{}





% \long\xdef\conditions{write the sell conditions here
% 
% on several lines}% FIXME





% the right shift of the right blocks
\newlength{\@right@alignment}
\setlength\@right@alignment{11cm}

% footnote style
\newcommand\footnotestyle[1]{%
  {\textsf{\color{footnotegray}\fontsize{3mm}{0mm}\selectfont #1}}%
}

% change color of footnote marks
\renewcommand\@makefntext[1]{%
  \parindent 1em\noindent
  \hb@xt@1.8em{%
  \hss\@textsuperscript{\normalfont\color{footnotegray}\@thefnmark}}#1}

\parindent=0pt

  
\AtBeginDocument{%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ifnum\pdf@shellescape=\@ne % check if --shell-escape option is enabled so that a customized jobname can be used for compilation (see https://tex.stackexchange.com/a/5265/46718)
    \def\my@job@name{\@@invoice@num_\@@invoice@type}
    \ifx\conditionmacro\undefined
    \immediate\write18{%
        pdflatex --jobname="\my@job@name"
        "\gdef\string\conditionmacro{1}\string\input\space\jobname"
    }%
    \expandafter\stop
    \fi
\fi % FIXME : warning : if the invoicenumber is not changed, the new invoice will overwrite the previous one


\hypersetup{pdftitle={\@@invoice@type ~\@@invoice@num}, pdfauthor={\@@company@name}, pdfsubject={\@@invoice@type}, pdfkeywords={\@@invoice@type}} 


\begin{minipage}[b]{\@right@alignment}%
\color{gray!95}\fontsize{1cm}{1cm}\selectfont %
\@@company@name%
\end{minipage}
%
\begin{minipage}[b]{0.35\textwidth}\ttfamily
\color{gray!95}\fontsize{1cm}{1cm}\selectfont %
\@@invoice@type% ``Facture'' or ``Devis''
\end{minipage}
\vskip 3mm%

\begin{minipage}[t][2cm][t]{\@right@alignment}%
{%\fontsize{0.44cm}{0.5cm}%
{\Large\@@company@subname }\\
\@@company@addressA \\
\@@company@addressB \\
T\'el : \@@company@tel \\
Email : \href{mailto:\@@company@email}{\@@company@email}
}
\end{minipage}
%
\begin{minipage}[t][2cm][t]{0.5\textwidth}{\ttfamily N\textsuperscript{o} \@@invoice@num \\
\@date }
%%%%%%%%%%%%%%%%%% Numero facture + date
\vskip 0.3cm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CLIENT 
\leavevmode \kern -3mm \colorbox{gray!85}{
  \kern 1mm\begin{minipage}[t]{\linewidth}
    \color{white} \bf
    \vskip 2mm
    {\Large \@@customer@name }\\
    \@@customer@addressA\\
    \@@customer@addressB
    \vspace*{1.5mm}%
  \end{minipage}
}
\end{minipage}

\vskip 5cm

} % END AT BEGIN DOCUMENT




\def\@@object{}
\def\@@command@total{0.00} % Initial Value
\let\origwrite\write

\NewDocumentEnvironment{objects}{+b}{

\newcommand*{\@add@to@total}[1]{
    \IfBooleanF\lTblrMeasuringBool{% avoid multiple calculations due to 'tabularray' internal work - need version 2023A or higher
        \FPeval\TempTotal{(\@@command@total)+##1}
        \xdef\@@command@total{\TempTotal}
    }{}
    }

\newcommand{\object}[3]{%
    \gdef\@object@name{##1}%
    \gdef\@object@price{##2}%
    \gdef\@object@qty{##3}%
    \FPeval\local@computation{##2*##3}%
    \global\let\@computation\local@computation % see https://tex.stackexchange.com/a/455571/46718
    \gdef\@object@total{\@computation} %
    \@add@to@total{\@object@total}% adding object total price to the total of the command
    \@object@name & \num{\@object@price}\,\texteuro & \@object@qty & \num{\@object@total}\,\texteuro \\%
    }


\begin{tblr}[expand=\object]
  {colspec = {X[6,l,m]X[1,c,m]X[0.5,c,m]X[1,r,m]},
  rowsep = {3mm},
  rowhead = 1, rowfoot = 1,
  row{odd} = {tablecolor1}, row{even} = {tablecolor2},
  hlines={white},
  row{1} = {12mm, c, bg=headcolor, font=\large\bfseries},
  row{Z} = {12mm, headcolor, font=\large\bfseries} % last line
  }
  Objet & {Prix\\ unit.} & Qté & Prix HT \\
  #1 % <-- Content of NewDocumentEnvironment
  {\hfill Total en Euros \\
  \hfill \footnotesize\normalfont  \emph{ TVA non applicable, art. 293B du CGI}}
    & & & \num{\@@command@total}\,\texteuro \\
\end{tblr}
\smallskip

}{} % end objects



\AtEndDocument{%%%%%%%%%%%%%%%%%%%%%%%
\vfill

\small

\setlength{\columnsep}{1.5cm}
\begin{multicols}{2}
%\begin{center}
\noindent\@@company@name,\\
%Auto-entrepreneur {\small (APE XXXXX)},\\
\@@company@addressA,\\
\@@company@addressB \\
%\@@company@tel, \@@company@email\\
\columnbreak

\noindent\hbox to 1.2cm{SIREN\,:\hss} \@@company@siren\\
\hbox to 1.2cm{IBAN\,:\hss} \@@company@iban\\
\hbox to 1.2cm{BIC\,:\hss} \@@company@bic \\
\end{multicols} 
%\end{center}

%\footnotestyle{\textbf{Conditions de paiement:} Si vous recevez cette facture, c'est que vous avez donn\'e votre accord sur le visuel. Aucun remboursement ni aucune modification ne poura \^etre demand\'e une fois que le fichier a \'et\'e envoy\'e. }





}% END AT END DOCUMENT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  
