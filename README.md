# Une classe LaTeX pour générer des factures

Ceci est une classe LaTeX pour générer des factures ou des devis facilement en LaTeX : `invoice.cls`.

Un exemple est donné dans le fichier `Invoice_exemple.tex`.

Par défaut, le fichier produit porte la mention *Facture*. L'option `devis` peut être entrée à l'appel de la classe (`\documentclass[devis]{invoice}` afin d'utiliser la mention *Devis* à la place.

Dans le préambule on donne les caractéristiques de l'entreprise vendeuse dans l'environnement `company`, et celles du client dans l'environnement `customer`, sans oublier un numéro de facture `invoicenumber`.

Dans le corps du document l'environnement `objects` permet de lister les articles, par la commande `\object` qui prend 3 arguments : la désignation de l'article, son prix unitaire en euros, et le nombre d'articles commandés.

Le calcul des sous-totaux et du total global est calculé automatiquement. (La prise en compte de la TVA n'est pas fait pour le moment)


## Compilation

La compilation avec `pdflatex` produit un fichier PDF qui portera le nom du fichier TeX utilisé, comme d'habitude.

La compilation en utilisant l'option `--shell-escape` de créer un second fichier PDF nommé d'après le numéro de facture entré dans `invoicenumber`. 


## Crédits et licence
Ce template se base sur [YAIT: yet another invoice template](https://github.com/eroux/latex-yait), par Elie Roux.


Il est réalisé par Globulle sous licence [CC-By 4.0](http://creativecommons.org/licenses/by/4.0/).
